using System.Text.RegularExpressions;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace kllmp.telegram.bot.servicio.Services;
public class ListenToMessages 
{
    public static Task ValidateText(ITelegramBotClient botClient, Message message)
    {
        return Task.Factory.StartNew(()=>
        {
            if (!string.IsNullOrEmpty(message.Text))
            {
                message.Text = message.Text.ToLower();
                var botClientUserName = botClient.GetMeAsync().Result.Username!.ToLower();

                if (message.Text == "/start")
                {
                    InitChatTelegram(message);
                }
                else if(Regex.IsMatch(message.Text, @"^/start \d*") || Regex.IsMatch(message.Text, $"^/start@{botClientUserName} \\d*"))
                {
                    InitChatTelegram(message);
                    RegisterFolio(message, botClientUserName, "/start");
                }
                else if (Regex.IsMatch(message.Text, @"^/registrar \d*") || Regex.IsMatch(message.Text, $"^/registrar@{botClientUserName} \\d*"))
                {
                    RegisterFolio(message, botClientUserName, "/registrar");
                }
            }
        });
        
    }

    private static void InitChatTelegram(Message message)
    {
        Console.WriteLine("InitChatTelegram");
    }

    private static void RegisterFolio(Message message, string botClientUserName, string strReplace)
    {
        string folio = message.Text!.Replace(strReplace, "").Trim();
        folio = folio.Replace($"@{botClientUserName}", "").Trim();
        int.TryParse(folio, out int userChatId);
        
        Console.WriteLine($"RegisterFolio => {folio}");

        // string message = "El tipo de notificación es invalido";
        // if (UserChatService.ExistsFolio(userChatId))
        // {
        //     if (!UserChatService.IsActive(userChatId))
        //     {
        //         if (UserChatService.IsPersonal(userChatId) == ItsPrivateChat(Message.Chat))
        //         {
        //             var chatTelegram = new ChatTelegramDataBaseService(Message.Chat);
        //             if (!chatTelegram.ExistsRecordInDB(true)) { message = Register(folio, Message.Chat.Id); }
        //             else { message = "Este dispositivo ya está registrado y activo"; }
        //         }
        //     }
        //     else { message = "El folio proporcionado ya se encuentra activo"; }
        // }
        // else { message = $"No existen registros para este folio: {folio}"; }
        // SendMessageText(message.Chat.Id, message);
        
    }
}