using Telegram.Bot;
using Telegram.Bot.Extensions.Polling;

namespace kllmp.telegram.bot.servicio.Services;

public class TelegramService : ITelegramService
{
    private IConfiguration _configuration { get; }
    private TelegramBotClient _bot;
    private readonly CancellationTokenSource cts = new CancellationTokenSource();
    private readonly ReceiverOptions receiverOptions = new() { AllowedUpdates = { } };
    public TelegramService(IConfiguration configuration)
    {
        _configuration = configuration;
        _bot = new TelegramBotClient(_configuration["tokenBot"]);
    }

    public Task StartAsync() => Task.Factory.StartNew(()=> _bot.StartReceiving(Handlers.HandleUpdateAsync,Handlers.HandleErrorAsync,receiverOptions,cts.Token));

    public Task StopAsync() => Task.Factory.StartNew(()=>  cts.Cancel());

}