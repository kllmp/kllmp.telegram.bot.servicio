namespace kllmp.telegram.bot.servicio.Services;

public interface ITelegramService
{
    public Task StartAsync();
    public Task StopAsync();
}