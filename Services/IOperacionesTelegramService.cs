using Telegram.Bot;
using Telegram.Bot.Types;

namespace kllmp.telegram.bot.servicio.Services;

public interface IOperacionesTelegramService
{
    public Task ValidarMenu(ITelegramBotClient botClient, CallbackQuery callbackQuery);
}