using kllmp.telegram.bot.servicio.Services;

namespace kllmp.telegram.bot.servicio;

public class Worker : BackgroundService
{
    private readonly ILogger<Worker> _logger;
    private readonly ITelegramService _telegramService;

    public Worker(ILogger<Worker> logger, ITelegramService telegramService)
    {
        _logger = logger;
        _telegramService = telegramService;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        await _telegramService.StartAsync();
    }

    public override async Task StopAsync(CancellationToken stoppingToken)
    {
        await _telegramService.StopAsync();
    }
}
