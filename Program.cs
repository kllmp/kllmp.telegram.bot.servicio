using kllmp.telegram.bot.servicio;
using kllmp.telegram.bot.servicio.Services;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        services.AddSingleton<ITelegramService, TelegramService>();
        services.AddHostedService<Worker>();
    })
    .Build();

await host.RunAsync();
